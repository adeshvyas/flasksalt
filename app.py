import csv
import datetime
from io import TextIOWrapper

import jwt as jwt
from flask import Flask, request, make_response, jsonify
import psycopg2
import psycopg2.extras
import secrets
import string
import hashlib
import logging
from functools import wraps

app = Flask(__name__)
DB_HOST = "172.17.0.1"
DB_NAME = "postgres"
DB_USER = "postgres"
DB_PASS = "root"

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)
app.config['SECRET_KEY'] = '47Billion'
could_not_verify_message = 'Could not verify'
error_occur_message = 'Error Occurred'
login_required_message = 'Basic realm="Login required!"'
match_fail_message = 'Basic realm="Match Failed!"'


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        logger.info(f"In decorated")
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
            logger.info(f"Token if: {token}")

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            JWT_ALGORITHM = 'HS256'
            data = jwt.decode(token, app.config['SECRET_KEY'], JWT_ALGORITHM)
            logger.info(f"Data from Header :{data}")
            current_user = data['user']
            role = data['role']
        except Exception as e:
            logger.info(f"Token in except : {token}")
            logger.error(f"Error message: {e}")
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(current_user, role, *args, **kwargs)

    return decorated


@app.route('/')
def hello_world():
    # login()
    return "Hello World"


@app.route('/unprotected')
def unprotected():
    return jsonify({'message': 'Anyone can view this'})


@app.route('/protected')
@token_required
def protected(current_user, role):
    logger.info(f"In protected AND Value of current user and Role :{current_user} {role}")
    if role in {"student", "admin"}:
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(" SELECT * FROM student;")
        student = cur.fetchone()
        logger.info(f"Values From DB after token authentication: {student}")
        student_data = str(student)
        student_data = student_data.split(",")
        logger.info(f"List after split : {student_data}")
        name = student_data[1]
        email = student_data[2]
        grade = student_data[5]
        subject = student_data[4]
        address = student_data[3]
        return jsonify(
            {'message': 'Only for people with tokens', "First Name": name, "Grade ": grade, "Address": address,
             "Subject": subject, "Email": email})
    else:
        return 'User type not allowed'


def salt_generator():
    string_source = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_lowercase)
    password += secrets.choice(string.ascii_uppercase)
    password += secrets.choice(string.digits)
    password += secrets.choice(string.punctuation)

    for _ in range(6):
        password += secrets.choice(string_source)

    char_list = list(password)
    secrets.SystemRandom().shuffle(char_list)
    password = ''.join(char_list)
    return password


@app.route('/register', methods=['POST'])
def register():
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    user_role_sql = "INSERT INTO user_login (user_id,user_email,salt,password,roles) VALUES (%s,%s,%s,%s,%s)"
    request_data = request.get_json()
    name = request_data['name']
    email = request_data['email']
    address = request_data['address']
    subject = request_data['subject']
    grade = request_data['grade']
    password = request_data['pass']
    role = request_data['role']
    salt = salt_generator()
    logger.info(f"email:{name} and password:{email} from postman")
    new_pass = password + salt
    logger.info(f"Salt :{salt}")
    db_pass = hashlib.sha256(new_pass.encode()).hexdigest()
    logger.info(f"Hashed Pass{db_pass}")
    if role == "student":
        cur.execute("INSERT INTO student (name,email,address,subject,grade) VALUES (%s,%s,%s,%s,%s)",
                    (name, email, address, subject, grade))
        conn.commit()
        cur.execute("SELECT student_id FROM student WHERE email = %s;", [email, ])
        role_id = cur.fetchone()
        user_id = str(role_id[0])
        cur.execute(user_role_sql,
                    (user_id, email, salt, db_pass, role))
        conn.commit()
        logger.info(f"Insertion in Student table Successful")
        return f'Inserted Successfully in student table'
    elif role == "teacher":
        cur.execute("INSERT INTO teacher (name,email,address,subject,grade) VALUES (%s,%s,%s,%s,%s)",
                    (name, email, address, subject, grade))
        conn.commit()
        cur.execute("SELECT teacher_id FROM teacher WHERE email = %s;", [email, ])
        role_id = cur.fetchone()
        user_id = str(role_id[0])
        cur.execute(user_role_sql, (user_id, email, salt, db_pass, role))
        conn.commit()
        logger.info(f"Insertion in Teacher table Successful")
        return f'Inserted Successfully in Teacher table'
    elif role == "admin":
        user_id = "admin101"
        cur.execute(user_role_sql,
                    (user_id, email, salt, db_pass, role))
        conn.commit()
        logger.info(f"Insertion in User table Successful for Admin")
        return f'Inserted Successfully in User table for Admin'

    else:
        logger.info("Invalid Role Type")
        return 'Invalid Role Type'


@app.route('/login')
def login():
    auth = request.authorization
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    au_email = auth.username
    au_password = auth.password
    logger.info(type(au_password))
    cur.execute(""" SELECT salt,roles FROM user_login WHERE user_email = %s;""", [au_email, ])
    data = cur.fetchone()
    salt = str(data[0])
    temp_role = str(data[1])
    logger.info(f"Salt from DB{salt} and Roles According to Email: {temp_role}")
    combined = au_password + salt
    secure_password = hashlib.sha256(combined.encode()).hexdigest()
    logger.info(f"Password after hash: {secure_password}")
    cur.execute(""" SELECT user_email,roles FROM user_login WHERE user_email = %s and password = %s and roles= %s;""",
                [au_email, secure_password, temp_role])
    success_data = cur.fetchone()
    email = str(success_data[0])
    roles = str(success_data[1])
    logger.info(f"User_mail : {email} and Roles: {roles}")
    logger.info(auth.username + "  " + auth.password)
    if not auth or not auth.username or not auth.password:
        return make_response(could_not_verify_message, 401, {'WWW-Authenticate': login_required_message})

    if email is None:
        return make_response(could_not_verify_message, 401, {'WWW-Authenticate': login_required_message})

    if email == au_email:
        JWT_ALGORITHM = 'HS256'
        token = jwt.encode(
            {'user': au_email, 'role': roles, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},
            app.config['SECRET_KEY'], JWT_ALGORITHM)
        logger.info(f"Token : {token}")
        return jsonify({'token': token})

    return make_response(could_not_verify_message, 401, {'WWW-Authenticate': login_required_message})


@app.route('/update_password', methods=['PUT'])
def update_password():
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    request_data = request.get_json()
    email = request_data['email']
    password = request_data['old_pass']
    new_pass = request_data['new_pass']
    cur.execute(" SELECT salt,roles FROM user_login WHERE user_email = %s;", [email, ])
    data = cur.fetchone()
    salt = str(data[0])
    temp_role = str(data[1])
    logger.info(f"Salt from DB{salt} and Roles According to Email: {temp_role}")
    combined = password + salt
    secure_password = hashlib.sha256(combined.encode()).hexdigest()
    logger.info(f"Password after hash OLD: {secure_password}")
    combined = new_pass + salt
    new_password = hashlib.sha256(combined.encode()).hexdigest()
    logger.info(f"Password after hash NEW: {new_password}")

    cur.execute("UPDATE user_login SET password = %s WHERE user_email = %s and password = %s and roles= %s;",
                [new_password, email, secure_password, temp_role])
    flag = cur.rowcount
    logger.info(f"Output of Update Query: {flag}")
    conn.commit()
    if flag == 0:
        return make_response(error_occur_message, 401, {'WWW-Authenticate': 'Basic realm="Login Failed!"'})
    else:
        return 'Updated Successfully'


@app.route('/student', methods=['PUT'])
@token_required
def update_student(current_user, role):
    if role in {"student", "admin"}:
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        request_data = request.get_json()
        name = request_data['name']
        email = request_data['email']
        address = request_data['address']
        subject = request_data['subject']
        grade = request_data['grade']
        if role == "student":
            cur.execute("UPDATE student SET name = %s, address = %s, subject = %s, grade = %s WHERE email = %s;",
                        [name, address, subject, grade, current_user])
            flag = cur.rowcount
            logger.info(f"Output of Update Query: {flag}")
            conn.commit()
            if flag == 0:
                return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
            else:
                return 'Student Data Updated Successfully By User'
        else:
            cur.execute("UPDATE student SET name = %s, address = %s, subject = %s, grade = %s WHERE email = %s;",
                        [name, address, subject, grade, email])
            flag = cur.rowcount
            logger.info(f"Output of Update Query: {flag}")
            conn.commit()
            if flag == 0:
                return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
            else:
                return 'Student Data Updated Successfully by Admin'
    else:
        return 'Invalid User to Update'


@app.route('/teacher', methods=['PUT'])
@token_required
def update_teacher(current_user, role):
    if role in {'teacher', 'admin'}:
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        request_data = request.get_json()
        name = request_data['name']
        email = request_data['email']
        address = request_data['address']
        subject = request_data['subject']
        grade = request_data['grade']
        if role == "teacher":
            cur.execute("UPDATE teacher SET name = %s, address = %s, subject = %s, grade = %s WHERE email = %s;",
                        [name, address, subject, grade, current_user])
            flag = cur.rowcount
            logger.info(f"Output of Update Query: {flag}")
            conn.commit()
            if flag == 0:
                return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
            else:
                return 'Teacher Data Updated Successfully by User'
        else:
            cur.execute("UPDATE teacher SET name = %s, address = %s, subject = %s, grade = %s WHERE email = %s;",
                        [name, address, subject, grade, email])
            flag = cur.rowcount
            logger.info(f"Output of Update Query: {flag}")
            conn.commit()
            if flag == 0:
                return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
            else:
                return 'Teacher Data Updated Successfully by Admin'


@app.route('/student/<string:email>', methods=['DELETE'])
@token_required
def delete_student(current_user, role, email):
    delete_from_role = 'DELETE FROM student WHERE email = %s;'
    delete_from_user = 'DELETE FROM user_login WHERE user_email = %s;'

    if role == "student":
        logger.info(f"Email from URL : {email}")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(delete_from_role, [current_user])
        conn.commit()
        cur.execute(delete_from_user, [current_user])
        conn.commit()
        flag = cur.rowcount
        logger.info(f"Output of Update Query: {flag}")
        conn.commit()
        if flag == 0:
            return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
        else:
            return 'Deleted Successfully by User'
    elif role == "admin":
        logger.info(f"Email from URL : {email}")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(delete_from_role, [email])
        conn.commit()
        cur.execute(delete_from_user, [email])
        conn.commit()
        flag = cur.rowcount
        logger.info(f"Output of Update Query: {flag}")
        conn.commit()
        if flag == 0:
            return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
        else:
            return 'Deleted Successfully by Admin'
    else:
        return 'invalid User Access'


@app.route('/teacher/<string:email>', methods=['DELETE'])
@token_required
def delete_teacher(current_user, role, email):
    delete_from_role = 'DELETE FROM teacher WHERE email = %s;'
    delete_from_user_table = 'DELETE FROM user_login WHERE user_email = %s;'
    if role == 'teacher':
        logger.info(f"Email from URL : {email}")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(delete_from_role, [current_user])
        conn.commit()
        cur.execute(delete_from_user_table, [current_user])
        conn.commit()
        flag = cur.rowcount
        logger.info(f"Output of Update Query: {flag}")
        conn.commit()
        if flag == 0:
            return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
        else:
            return 'Deleted Successfully by Teacher'

    elif role == 'admin':
        logger.info(f"Email from URL : {email}")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(delete_from_role, [email])
        conn.commit()
        cur.execute(delete_from_user_table, [email])
        conn.commit()
        flag = cur.rowcount
        logger.info(f"Output of Update Query: {flag}")
        conn.commit()
        if flag == 0:
            return make_response(error_occur_message, 401, {'WWW-Authenticate': match_fail_message})
        else:
            return 'Deleted Successfully by Admin'
    else:
        return 'Invalid User Type'


@app.route('/marks', methods=['POST'])
@token_required
def upload_marks(current_user, role):
    if role in ('admin', 'teacher'):
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logger.info("Inside upload Module")
        csv_file = request.files['file']
        logger.info(f"File : {csv_file}")
        csv_file = TextIOWrapper(csv_file, encoding='utf-8')
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            enroll, a_subject, b_subject, grades = row
            sql = "INSERT INTO student_marks (student_enroll,a_subject,b_subject,grades, uploader_id) " \
                  "VALUES (%s,%s,%s,%s,%s)"
            cur.execute(sql, [enroll, a_subject, b_subject, grades, current_user])
        conn.commit()

        return "Marks Uploaded Successfully"
    else:
        return 'Invalid User Type'


@app.route('/result', methods=['GET'])
@token_required
def result(current_user, role):
    if role == 'student':
        logger.info("Inside Result Module")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(" SELECT student_id from student WHERE email= %s;", [current_user, ])
        student_id = cur.fetchone()
        logger.info(f"Student id: {student_id}")
        student_enroll = str(student_id[0])
        cur.execute(
            "SELECT student_enroll, a_subject, b_subject, grades from student_marks WHERE student_enroll= %s;",
            [student_enroll, ])
        row = cur.fetchone()
        return jsonify(
            {'message': 'Results are here', "Student Enrollment :": row[0], "Physics ": row[1], "Chemistry ": row[2]})
    else:
        return "No Results Found"


@app.route('/report', methods=['GET'])
@token_required
def report(current_user, role):
    if role in ('teacher', 'admin'):
        logger.info("Inside Report Module")
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "SELECT student_enroll, a_subject, b_subject, grades from student_marks ;")
        row = cur.fetchall()
        result = []
        if row:
            for tuples in row:
                result.append({
                    'Enrollment Number ': tuples[0],
                    'Physics': tuples[1],
                    'Chemistry': tuples[2]

                })
        else:
            return "NO Results Uploaded yet"

        logger.info(f"Fetched Report Card : {row}")
        return jsonify(
            {'Report Cards ': result})
    else:
        return "Not Allowed"


if __name__ == '__main__':
    app.run()
